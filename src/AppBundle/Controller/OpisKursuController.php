<?php

namespace AppBundle\Controller;

use AppBundle\Entity\OpisKursu;
use AppBundle\Entity\Kurs;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Opiskursu controller.
 *
 * @Route("opiskursu")
 */
class OpisKursuController extends Controller
{
    /**
     * Lists all opisKursu entities.
     *
     * @Route("/", name="opiskursu_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $opisKursus = $em->getRepository('AppBundle:OpisKursu')->findAll();

        return $this->render('opiskursu/index.html.twig', array(
            'opisKursus' => $opisKursus,
        ));
    }

    /**
     * Creates a new opisKursu entity.
     *
     * @Route("/new", name="opiskursu_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $opisKursu = new Opiskursu();
        $form = $this->createForm('AppBundle\Form\OpisKursuType', $opisKursu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($opisKursu);
            $em->flush();

            return $this->redirectToRoute('opiskursu_show', array('id' => $opisKursu->getId()));
        }

        return $this->render('opiskursu/new.html.twig', array(
            'opisKursu' => $opisKursu,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a opisKursu entity.
     *
     * @Route("/{id}", name="opiskursu_show")
     * @Method("GET")
     */
    public function showAction(OpisKursu $opisKursu)
    {
        $deleteForm = $this->createDeleteForm($opisKursu);

        return $this->render('opiskursu/show.html.twig', array(
            'opisKursu' => $opisKursu,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing opisKursu entity.
     *
     * @Route("/{id}/edit", name="opiskursu_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, OpisKursu $opisKursu)
    {
        $deleteForm = $this->createDeleteForm($opisKursu);
        $editForm = $this->createForm('AppBundle\Form\OpisKursuType', $opisKursu);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('opiskursu_edit', array('id' => $opisKursu->getId()));
        }

        return $this->render('opiskursu/edit.html.twig', array(
            'opisKursu' => $opisKursu,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a opisKursu entity.
     *
     * @Route("/{id}", name="opiskursu_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, OpisKursu $opisKursu)
    {
        $form = $this->createDeleteForm($opisKursu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($opisKursu);
            $em->flush();
        }

        return $this->redirectToRoute('opiskursu_index');
    }

    /**
     * Creates a form to delete a opisKursu entity.
     *
     * @param OpisKursu $opisKursu The opisKursu entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(OpisKursu $opisKursu)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('opiskursu_delete', array('id' => $opisKursu->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
