<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Kursy;
use AppBundle\Entity\OpisKursu;
use AppBundle\Events;
use AppBundle\Form\CommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Kursy controller.
 *
 * @Route("kursy")
 */
class KursyController extends Controller
{
    /**
     * Lists all kursy entities.
     *
     * @Route("/", name="kursy_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $kursies = $em->getRepository('AppBundle:Kursy')->findAll();

        return $this->render('kursy/index.html.twig', array(
            'kursies' => $kursies,
        ));
    }

    /**
     * Creates a new kursy entity.
     *
     * @Route("/new", name="kursy_new")
     * 
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $kursy = new Kursy();
        $form = $this->createForm('AppBundle\Form\KursyType', $kursy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($kursy);
            $em->flush();

            return $this->redirectToRoute('kursy_show', array('id' => $kursy->getId()));
        }

        return $this->render('kursy/new.html.twig', array(
            'kursy' => $kursy,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a kursy entity.
     *
     * @Route("/{id}", name="kursy_show")
     * @Method("GET")
     */
    public function showAction(Kursy $kursy)
    {
        $deleteForm = $this->createDeleteForm($kursy);
        $repository = $this->getDoctrine()->getRepository(OpisKursu::class);
        $opisy = $repository->findByKursy($kursy);
        return $this->render('kursy/show.html.twig', array(
            'kursy' => $kursy,
            'delete_form' => $deleteForm->createView(),
            'opisy' => $opisy,
        ));
    }

    /**
     * Displays a form to edit an existing kursy entity.
     *
     * @Route("/{id}/edit", name="kursy_edit")
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Kursy $kursy)
    {
        $deleteForm = $this->createDeleteForm($kursy);
        $editForm = $this->createForm('AppBundle\Form\KursyType', $kursy);
        $opis =  new OpisKursu();
        $opisForm = $this->createForm('AppBundle\Form\OpisKursuType', $opis);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('kursy_edit', array('id' => $kursy->getId()));
        }

        return $this->render('kursy/edit.html.twig', array(
            'kursy' => $kursy,
            'edit_form' => $editForm->createView(),
            'opis_form' => $opisForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a kursy entity.
     *
     * @Route("/{id}", name="kursy_delete")
     * @Security("is_granted('ROLE_ADMIN')")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Kursy $kursy)
    {
        $form = $this->createDeleteForm($kursy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($kursy);
            $em->flush();
        }

        return $this->redirectToRoute('kursy_index');
    }

    /**
     * @Route("/opis/{id}/new", name="opiskursu_new")
     * @Method("POST")
     * @Security("is_granted('ROLE_ADMIN')")
     * @ParamConverter("kursy", options={"mapping": {"kursySlug": "slug"}})
     *
     * NOTE: The ParamConverter mapping is required because the route parameter
     * (postSlug) doesn't match any of the Doctrine entity properties (slug).
     * See https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/annotations/converters.html#doctrine-converter
     */
    public function opisKursuNewAction(Request $request, Kursy $kursy, EventDispatcherInterface $eventDispatcher)
    {
        $opis = new OpisKursu();
        $kursy->addComment($opis);

        $form = $this->createForm(CommentType::class, $opis);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($opis);
            $em->flush();

            // When triggering an event, you can optionally pass some information.
            // For simple applications, use the GenericEvent object provided by Symfony
            // to pass some PHP variables. For more complex applications, define your
            // own event object classes.
            // See https://symfony.com/doc/current/components/event_dispatcher/generic_event.html
            $event = new GenericEvent($opis);

            // When an event is dispatched, Symfony notifies it to all the listeners
            // and subscribers registered to it. Listeners can modify the information
            // passed in the event and they can even modify the execution flow, so
            // there's no guarantee that the rest of this controller will be executed.
            // See https://symfony.com/doc/current/components/event_dispatcher.html
            $eventDispatcher->dispatch(Events::COMMENT_CREATED, $event);

            return $this->redirectToRoute('kursy_show', ['slug' => $kursy->getSlug()]);
        }

        return $this->render('blog/comment_form_error.html.twig', [
            'post' => $kursy,
            'form' => $form->createView(),
        ]);
    }

    /**
     * This controller is called directly via the render() function in the
     * blog/post_show.html.twig template. That's why it's not needed to define
     * a route name for it.
     *
     * The "id" of the Post is passed in and then turned into a Post object
     * automatically by the ParamConverter.
     *
     * @param Kursy $kursy
     *
     * @return Response
     */

    public function opisKursuFormAction(OpisKursu $opis)
    {
        $form = $this->createForm(OpisKursuType::class);

        return $this->render('kursy/_opisKursu_form.html.twig', [
            'post' => $opis,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a kursy entity.
     *
     * @param Kursy $kursy The kursy entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Kursy $kursy)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('kursy_delete', array('id' => $kursy->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
