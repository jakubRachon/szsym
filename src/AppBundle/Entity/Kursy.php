<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Kursy
 *
 * @ORM\Table(name="kursy")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\KursyRepository")
 */
class Kursy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nazwa", type="string", length=255)
     */
    private $nazwa;

    /**
     * @var string
     *
     * @ORM\Column(name="cena", type="string", length=255)
     */
    private $cena;

    /**
     * @var string
     *
     * @ORM\Column(name="czastrwania", type="string", length=255)
     */
    private $czastrwania;

    /**
     * ToString method
     *
     * @return String
     */


    public function __toString()
    {
        return $this->nazwa;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nazwa
     *
     * @param string $nazwa
     *
     * @return Kursy
     */
    public function setNazwa($nazwa)
    {
        $this->nazwa = $nazwa;

        return $this;
    }

    /**
     * Get nazwa
     *
     * @return string
     */
    public function getNazwa()
    {
        return $this->nazwa;
    }

    /**
     * Set cena
     *
     * @param string $cena
     *
     * @return Kursy
     */
    public function setCena($cena)
    {
        $this->cena = $cena;

        return $this;
    }

    /**
     * Get cena
     *
     * @return string
     */
    public function getCena()
    {
        return $this->cena;
    }

    /**
     * Set czastrwania
     *
     * @param string $czastrwania
     *
     * @return Kursy
     */
    public function setCzastrwania($czastrwania)
    {
        $this->czastrwania = $czastrwania;

        return $this;
    }

    /**
     * Get czastrwania
     *
     * @return string
     */
    public function getCzastrwania()
    {
        return $this->czastrwania;
    }
}
