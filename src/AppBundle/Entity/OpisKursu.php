<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Entity\Kursy;

/**
 * OpisKursu
 *
 * @ORM\Table(name="opis_kursu")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OpisKursuRepository")
 */
class OpisKursu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Kursy
     *
     * @ORM\ManyToOne(targetEntity="Kursy")
     * @ORM\JoinColumn(nullable=true)
     */
    private $kursy;

    /**
     * @var string
     *
     * @ORM\Column(name="streszcz", type="string", length=255)
     */
    private $streszcz;

    /**
     * @var string
     *
     * @ORM\Column(name="opis", type="text", nullable=true)
     */
    private $opis;

    /**
     * @var string
     *
     * @ORM\Column(name="agenda", type="text", nullable=true)
     */
    private $agenda;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set streszcz
     *
     * @param string $streszcz
     *
     * @return OpisKursu
     */
    public function setStreszcz($streszcz)
    {
        $this->streszcz = $streszcz;

        return $this;
    }

    /**
     * Get streszcz
     *
     * @return string
     */
    public function getStreszcz()
    {
        return $this->streszcz;
    }

    /**
     * Set opis
     *
     * @param string $opis
     *
     * @return OpisKursu
     */
    public function setOpis($opis)
    {
        $this->opis = $opis;

        return $this;
    }

    /**
     * Get opis
     *
     * @return string
     */
    public function getOpis()
    {
        return $this->opis;
    }

    /**
     * Set agenda
     *
     * @param string $agenda
     *
     * @return OpisKursu
     */
    public function setAgenda($agenda)
    {
        $this->agenda = $agenda;

        return $this;
    }

    /**
     * Get agenda
     *
     * @return string
     */
    public function getAgenda()
    {
        return $this->agenda;
    }

    /**
     * Get Kurs
     *
     * @return int
     */
    public function getKursy()
    {
        return $this->kursy;
    }

    /**
     * Set agenda
     *
     * @param integer $kursy
     *
     * @return OpisKursu
     */
    public function setKursy($kursy)
    {
        $this->kursy = $kursy;

        return $this;
    }


}
